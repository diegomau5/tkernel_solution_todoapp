import requests
import pandas as pd
# Functions to iteract with APIRest.

#Function:
#Inputs: format [string] (default:'txt')
#Task: make a get request to get all todo task from Node API
#Return:
# if format = txt --> return a separated text by \n
# if format = json --> return the itself json response
# if format = table --> return a dataframe using pandas
# in failed case return None
def getAll(format="txt"):
    headers = {'Accept': 'application/json'}
    try:
        response = requests.get('http://192.168.1.20:8080/api/todos', headers=headers)
    except Exception as e:
        print(str(e))
        return None
    if response.status_code==200:
        response = response.json()
        if format=="txt":
            text=""
            for todo in response:
                text+=todo["text"]+"\n"
                response = text
        elif format=="table":
            response = pd.DataFrame(response)
    else:
        return None
    return response

#Function:
#Inputs: todo [string] // ej: 'hacer la cama'
#Task: make a post request to add a single todo task
#Return:
# True if the request returns a 200 status code or false if is a bad requests
def createTodo(todo):
    headers = {'Accept': 'application/json'}
    body = {"text":todo}
    try:
        response = requests.post('http://192.168.1.20:8080/api/todos', headers=headers,json=body)
        if response.status_code!=200:
            return False
        return True
    except Exception as e:
        print(str(e))
        return False

#Function:
#Inputs: id_todo [string] // ej: '64dsa5d4as5d4sa5d45'
#Task: make a post request to delete a single todo task by _id
#Return:
# True if the request returns a 200 status code or false if is a bad requests
def deleteTodo(id_todo):
    try:
        response = requests.delete('http://192.168.1.20:8080/api/todos/'+id_todo)
        if response.status_code!=200:
            return False
        return True
    except Exception as e:
        print(str(e))
        return False