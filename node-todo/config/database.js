// module.exports = {
//     remoteUrl : 'mongodb://root:rootpassword@192.168.1.20:27017/app'
//     // localUrl: 'mongodb://localhost/meanstacktutorials'
// };

const {
    DB_USER,
    DB_PASSWORD,
    DB_HOST,
    DB_NAME,
  } = process.env;
  
  module.exports = {
    remoteUrl: `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:27017/${DB_NAME}?authSource=admin`
  };