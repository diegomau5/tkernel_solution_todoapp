# Telefonica Kernel Challenge Solution

#### Solution provided by Diego Gutierrez Blanco

### Challenger check list:
- Dockerize the APP: **OK**
- Develop a CLI TOOL to interact with the application: **OK**
	- It must provide a command to get all the stored Todos in different
	formats selected by parameter: txt, table, json.
	- It must provide a command to add a single Todo.
	- It must provide a command to delete a single Todo.
	- It must provide a command to execute the following test workflow:
		- Create a random number of Todos between 10 and 100
		- Get all the Todos in JSON format
		- Get all the Todos in text format
		- Get all the Todos in table format
		- Clean all the stored Todos.

## How deploy the solution?
### Requirements:
----
- Docker and docker-compose installed
- Python Virtual Environment with version > 3.7 (version used in the development Python 3.10.6)

### Steps to deploy the solution:
---
#### Pull repository from Gitlab
Create a directory:

    mkdir todo_app
    cd todo_app/
    git clone https://gitlab.com/diegomau5/tkernel_solution_todoapp.git .

#### Run Node Server APP
From the same directory:
First, you must build the server image using the _Dockerfile_
Run the following command:

    docker build . -t node-todo-app

When the build process ends, you can follow with the next step, deploy the stack.
Before the deploy you need create a file _.env_ where you will declare database init variables
*Example:*

    #./.env
    MONGO_INIT_USER=root
    MONGO_INITDB_PASSWORD=rootpassword
    MONGO_DATABASE=todo_db

To deploy the stack you will run:

    docker-compose up -d

If you check the logs about the deploy process you can run:

    docker-compose logs -f

View in browser at `http://localhost:8080` to check that the server is running correctly.

#### Run CLI Tool
The **CLI TOOL** is developed in Python, so to execute it you must activate a Python Virtual Environment
Run:

    source <folder_env>/bin/activate
after that you must install the requirements file:
Run:

    pip install -r requirements.txt
Then you can run the app.

You have to go to the next directory:

    cd ./clitool/tool
from here you can start to use the cli tool.

#### Use Example:
##### Run help command:

    python app.py --help

*Response:*

     $ python app.py --help
     
    Usage: app.py [OPTIONS] COMMAND [ARGS]...  
      
    Welcome! TODO CLI Tool  
      
    ╭─ Options ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮  
    │  --install-completion  [bash|zsh|fish|powershell|pwsh] Install completion for the specified shell. [default: None]  │  
    │  --show-completion  [bash|zsh|fish|powershell|pwsh] Show completion for the specified shell, to copy it or customize the installation. [default: None]  │  
    │  --help  Show this message and exit. │  
    ╰──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯  
    ╭─ Commands ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮  
    │  createtodo Add a single todo task. Ej:// python createtodo 'hacer la cama' │  
    │  deletetodo Delete a single todo task by _id. Ej:// python deletetodo 640b58b731be33e5959fdaea │  
    │  getall return all todos. You can select return format using: │  
    │  testapp Run a stack of proof to test if app works. │  
    ╰──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯

##### Run getall command:
Command to get all the stored Todos in different formats selected by parameter: txt, table, json.

    $ python app.py getall  
    hacer la cama  
    hacer la compra
    $ python app.py getall json  
    [{'text': 'hacer la cama', '_id': '640b89f8e23b20470853693d', '__v': 0}, {'text': 'hacer la compra', '_id': '640b89fde23b202baf536940', '__v': 0}]  
    $ python app.py getall table  
    +----+-----------------+--------------------------+-------+  
    |    |     text         |        _id           | __v      |  
    |----+-----------------+--------------------------+-------|  
    | 0 | hacer la cama | 640b89f8e23b20470853693d | 0 |  
    | 1 | hacer la compra | 640b89fde23b202baf536940 | 0 |  
    +----+-----------------+--------------------------+-------+

##### Run createtodo command:
Command to add a single Todo.

    $ python app.py createtodo 'limpiar cocina y salon'  
    todo: 'limpiar cocina y salon' has been added sucessfully  
    $ python app.py getall  
    hacer la cama  
    hacer la compra  
    limpiar cocina y salon

##### Run deletetodo command:
Command to delete a single Todo (*by _id*)

    $ python app.py deletetodo 640b89fde23b202baf536940  
    todo: '640b89fde23b202baf536940' has been deleted sucessfully  
    $ python app.py getall  
    hacer la cama  
    limpiar cocina y salon


##### Run testapp command:
Command to execute the following test workflow:
i. Create a random number of Todos between 10 and 100
ii. Get all the Todos in JSON format
iii. Get all the Todos in text format
iv. Get all the Todos in table format
v. Clean all the stored Todos.

*(This command is better run it with the app server without todo created)*

    $ python app.py testapp  
    Test 1: Create a random number of Todos between 10 and 100: Success  
    Test 2: Get all the Todos in JSON format: Success  
    Test 3: Get all the Todos in TEXT format: Success  
    +----+--------+--------------------------+-------+  
    | | text | _id | __v |  
    |----+--------+--------------------------+-------|  
    | 0 | todo 0 | 640b8d77e23b208a48536956 | 0 |  
    | 1 | todo 1 | 640b8d77e23b20b5fa536959 | 0 |  
    | 2 | todo 2 | 640b8d77e23b207b7053695c | 0 |  
    | 3 | todo 3 | 640b8d77e23b204f3b53695f | 0 |  
    | 4 | todo 4 | 640b8d77e23b2077a8536962 | 0 |  
    +----+--------+--------------------------+-------+  
    Test 4: Get all the Todos in TABLE format: Success  
    Test 5: Clean all the stored Todos: Success

