import sys
sys.path.append("./")
import api
#Package to deploy a easily cli tool in python
import typer
# Package to generate a string table from pandas
from tabulate import tabulate
import random
import pandas as pd

# Typer app constructor
app = typer.Typer(help="Welcome!\nTODO CLI Tool")

# Funcion getAll -> return all todos in several formats
# Input -> format : string (default:'txt')
# Return -> response from api call function api.getAll(format)
@app.command()
def getAll(format: str = typer.Argument("txt")):
    """
     return all todos. You can select return format using:\n
     Format Arguments: txt,json,table\n
     Ej:// python getall txt
    """
    res=api.getAll(format)
    if format !="table":
        if res:
            if format=="json":
                print(res)
            else:
                print(res)
        else:
            print("Sorry, we can't execute your query now. PLease try later")
    else: 
        print(tabulate(res, headers='keys', tablefmt='psql'))
    

# Funcion createTodo -> Create a single todo task
# Input -> todo : string (string task to add)
# Return -> from api call function api.createTodo(todo) which returns True or False the function return print a success or failed text
@app.command()
def createTodo(todo: str):
    """
     Add a single todo task.
     Ej:// python createtodo 'hacer la cama'
    """
    res = api.createTodo(todo)
    if res: print("todo: '" + todo + "' has been added sucessfully")
    else: res: print("Sorry, we can't add your todo: '" + todo + "' now, please try later")
    
# Funcion createTodo -> delete a single todo task
# Input -> todo : string (id todo task to add)
# Return -> from api call function api.deleteTodo(todo) which returns True or False the function return print a success or failed text

@app.command()
def deleteTodo(todo: str):
    """
     Delete a single todo task by _id.
     Ej:// python deletetodo 640b58b731be33e5959fdaea
    """
    res = api.deleteTodo(todo)
    if res: print("todo: '" + todo + "' has been deleted sucessfully")
    else: res: print("Sorry, we can't delete your todo: '" + todo + "' now, please try later")


#Function to run a test workflow testing all app methods
@app.command()
def testApp():
    """
    Run a stack of proof to test if app works.  \n
    i. Create a random number of Todos between 10 and 100 \n
    ii. Get all the Todos in JSON format \n
    iii. Get all the Todos in text format \n
    iv. Get all the Todos in table format \n
    v. Clean all the stored Todos. \n
    
    Run: python app.py testapp
    """

    # Test 1: Check if the number of todos created is equal than number_of_todos generated randomly
    number_of_todos = random.randint(10,100)
    for num in range(0,number_of_todos):
        todo = "todo "+str(num)
        res = api.createTodo(todo)
    res = len(api.getAll("json"))
    if res == number_of_todos: 
        print("Test 1: Create a random number of Todos between 10 and 100: Success")
    else: print("Test 1: Create a random number of Todos between 10 and 100: Failed")
    
    #Test 2: Check if the response returned in json format has a size equal than number of todos
    res=api.getAll("json")
    if res:
        if len(res) == number_of_todos:
            print("Test 2: Get all the Todos in JSON format: Success")
        else:print("Test 2: Get all the Todos in JSON format: Failed")
    else:print("Test 2: Get all the Todos in JSON format: Failed")
    
    #Test 3: Check if the response is trying split the string by '\n' to assurance the string format and compare de len of that split
    # with number of todos
    res=api.getAll("txt")
    if res:
        if len(res.split("\n"))-1 == number_of_todos:
            print("Test 3: Get all the Todos in TEXT format: Success")
        else:print("Test 3: Get all the Todos in TEXT format: Failed")
    else:print("Test 3: Get all the Todos in TEXT format: Failed")
    
    #Test 4: Check if return from api getAll("table") is a table format checking the variable is a instance type of pandas to assurance
    # the table format 
    res=api.getAll("table")
    if isinstance(res, pd.DataFrame):
        print(tabulate(res[0:5], headers='keys', tablefmt='psql'))
        print("Test 4: Get all the Todos in TABLE format: Success")
    else:print("Test 4: Get all the Todos in TABLE format: Failed")
    
    #Test 5: Test delete function making a request to get all todos and then try remove it using the api. 
    # Check if after try remove the response from getAll is equal than zero.
    res=api.getAll("json")
    for todo in res:
        api.deleteTodo(todo["_id"])
    if len(api.getAll("json")) == 0:
        print("Test 5: Clean all the stored Todos: Success")
    else:
        print("Test 5: Clean all the stored Todos: Failed")

        
if __name__ == "__main__":
    app()