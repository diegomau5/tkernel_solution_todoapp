FROM node:18-alpine
WORKDIR /app
COPY ./node-todo/package.json .
RUN npm install
COPY ./node-todo .
CMD [ "node", "server.js" ]

